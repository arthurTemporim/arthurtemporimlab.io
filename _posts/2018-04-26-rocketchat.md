---
layout: null
title: April 2018
subtitle:
image: "img/timeline/rocketchat.jpg"
---
I participated in a summer internship at Rocket.Chat to develop 3 connectors
from different chatbot development platforms for integration with Rocket.Chat.
Here are their links:
[Rasa PR with connector](https://github.com/RasaHQ/rasa_core/pull/462)
[Botkit Rocket.Chat connector](https://github.com/RocketChat/botkit-rocketchat-connector)
[Botpress Rocket.Chat connector](https://github.com/RocketChat/botpress-channel-rocketchat)
